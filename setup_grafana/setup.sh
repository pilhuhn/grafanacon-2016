#!/bin/sh

set -x

G_HOST=grafana
G_PORT=3000

# Wait until Grafana is up and running
EC=1
while [ $EC -gt 0 ]
do
  sleep 5
  curl http://admin:admin@${G_HOST}:${G_PORT}/api/datasources -H "Content-Type: application/json"
  EC=$?
done


# Define the datasource 
curl http://admin:admin@${G_HOST}:${G_PORT}/api/datasources -X POST -H "Content-Type: application/json" --data-binary @ds-def.json

# Define a dashboard
curl http://admin:admin@${G_HOST}:${G_PORT}/api/dashboards/db -X POST -H "Content-Type: application/json" --data-binary @wildfly_usage_def.json
